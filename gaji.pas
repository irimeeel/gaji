program gaji1;
uses crt;

const
   max_Ttunja = 3;
   max_Tgapok = 3;
   bts_gapok = 300000;
   max_data_karyawan = 5;
type
  Ttunja = record
             status : string;
             tunja  : integer;
           end;

  TArr_Ttunja = array [1..max_Ttunja] of Ttunja;

  Tgapok = record
             gol   : char;
             gapok : integer;
             tunja : Tarr_Ttunja;
           end;

  Tarr_Tgapok = array [1..max_Tgapok] of Tgapok;

  Tgapok_tunja = record
                    gapok:integer;
                    tunja:integer;
                 end;

  Tdata_karyawan = record
                     nama       : string;
                     gol        : char;
                     status     : string;
                     gapok_tunja: Tgapok_tunja;
                     pot_iuran  : real;
                     gaber      : real;
                   end;

  Tarr_tdata_karyawan = array [1..max_data_karyawan] of Tdata_karyawan;

  procedure isi_data_gapok(var data_gapok:Tarr_Tgapok);forward;
  function cari_gapok_tunja(data_gapok:Tarr_Tgapok;gol:char;status:string):Tgapok_tunja;forward;
  function cari_data_karyawan(nama:string;data_karyawan:Tarr_tdata_karyawan;jml_data:integer):integer;forward;
  function fprosentase(gapok:integer):real;forward;
  function fpot_iuran(gapok_tunja:Tgapok_tunja;prosen:real):real;forward;
  function fgaber(gapok_tunja:Tgapok_tunja;pot_iuran:real):real;forward;
  procedure proses(data_gapok:Tarr_Tgapok;var data_karyawan:Tdata_karyawan);forward;
  procedure input(data_gapok:Tarr_Tgapok;var data_karyawan:Tarr_tdata_karyawan;var jml_data:integer);forward;
  procedure edit(data_gapok:Tarr_Tgapok;var data_karyawan:Tarr_tdata_karyawan;jml_data:integer);forward;
  procedure delete(var data_karyawan:Tarr_tdata_karyawan;var jml_data:integer);forward;
  procedure output(data_karyawan:Tarr_tdata_karyawan;jml_data:integer);forward;
  function menu:char;forward;

procedure isi_data_gapok(var data_gapok:Tarr_Tgapok);
begin
 with data_gapok[1] do
 begin
    gol := 'A';
    gapok := 200000;
    with data_gapok[1].tunja[1] do
    begin
       status := 'Belum';
       tunja  := 25000;
    end;
    with data_gapok[1].tunja[2] do
    begin
       status := 'Nikah';
       tunja  := 50000;
    end;
 end;

 with data_gapok[2] do
 begin
    gol := 'B';
    gapok := 350000;
    with data_gapok[2].tunja[1] do
    begin
       status := 'Belum';
       tunja  := 60000;
    end;
    with data_gapok[2].tunja[2] do
    begin
       status := 'Nikah';
       tunja  := 75000;
    end;
 end;
 
 with data_gapok[3] do
 begin
    gol := 'C';
    gapok := 400000;
    with data_gapok[3].tunja[1] do
    begin
       status := 'Belum';
       tunja  := 95000;
    end;
    with data_gapok[3].tunja[2] do
    begin
       status := 'Nikah';
       tunja  := 100000;
    end;
 end;
end;

function cari_gapok_tunja(data_gapok:Tarr_Tgapok;gol:char;status:string):Tgapok_tunja;
var
  i,j:integer;
  ketemu_gol,ketemu_status : boolean;
begin
   with cari_gapok_tunja do
   begin
         gapok:=0;
         tunja:=0;
   end;

   i:=1;
   ketemu_gol:=false;
   while (i<=max_Tgapok) and (not ketemu_gol) do
   begin
      ketemu_gol := data_gapok[i].gol=gol;
      if ketemu_gol then
      begin
         cari_gapok_tunja.gapok:=data_gapok[i].gapok;
         j:=1;
         ketemu_status:=false;
         while (j<=max_Ttunja) and (not ketemu_status) do
         begin
           ketemu_status := data_gapok[i].tunja[j].status=status;
           if ketemu_status then
              cari_gapok_tunja.tunja := data_gapok[i].tunja[j].tunja;
           j:=j+1;
         end;
      end;
      i:=i+1;
   end;
end;

function cari_data_karyawan(nama:string;data_karyawan:Tarr_tdata_karyawan;jml_data:integer):integer;
var
  i:integer;
  ketemu_nama:boolean;
begin
    cari_data_karyawan:=0;
    i:=1;
    ketemu_nama:=false;
    while (not ketemu_nama) and (i<=jml_data)  do
    begin
      ketemu_nama:= data_karyawan[i].nama=nama;
      if ketemu_nama then
         cari_data_karyawan:=i;
      i:=i+1;
    end;
end;

function fprosentase(gapok:integer):real;
begin
  fprosentase:=0;
  if gapok>bts_gapok then
      fprosentase:=0.1
   else
      if gapok <= bts_gapok then
         fprosentase := 0.05;
end;

function fpot_iuran(gapok_tunja:Tgapok_tunja;prosen:real):real;
begin
   fpot_iuran :=(gapok_tunja.gapok+gapok_tunja.tunja) * prosen;
end;

function fgaber(gapok_tunja:Tgapok_tunja;pot_iuran:real):real;
begin
   fgaber :=(gapok_tunja.gapok+gapok_tunja.tunja)-pot_iuran;
end;

procedure proses(data_gapok:Tarr_Tgapok;var data_karyawan:Tdata_karyawan);
begin
   with data_karyawan do
   begin
      gapok_tunja:=cari_gapok_tunja(data_gapok,gol,status);
      pot_iuran := fpot_iuran(gapok_tunja,fprosentase(gapok_tunja.gapok));
      gaber:=fgaber(gapok_tunja,pot_iuran);
   end;
end;

procedure input(data_gapok:Tarr_Tgapok;var data_karyawan:Tarr_tdata_karyawan;var jml_data:integer);
var
  i:integer;
  input_data : char;
begin
   clrscr;
  writeln('*****************');
  writeln('*  Input  Data  *');
  writeln('*****************');
   i:=jml_data+1;
     repeat
        writeln;
        writeln('Data ke-',i);
        with data_karyawan[i] do
        begin
           write('Nama Karyawan         :');readln(nama);
           write('Golongan (A\B\C)      :');readln(gol);
           write('Satus (Nikah\Belum)   :');readln(status);
        end;

        proses(data_gapok,data_karyawan[i]);

        if(i<max_data_karyawan) then
        begin
         writeln('Input data lagi (y/n) : ');readln(input_data);
        end;

        i:=i+1;
     until((input_data='n') or (input_data='N') or (i>max_data_karyawan));
     jml_data:=i-1;
end;

procedure edit(data_gapok:Tarr_Tgapok;var data_karyawan:Tarr_tdata_karyawan;jml_data:integer);
var
  input_data : char;
  nama : string;
  idx:integer;
begin
  clrscr;
  writeln('*****************');
  writeln('*  Edit  Data   *');
  writeln('*****************');
     repeat
        writeln;
        write('Nama Karyawan         :');readln(nama);
        idx:=cari_data_karyawan(nama,data_karyawan,jml_data);

        if idx=0 then
        begin
           writeln('**********************');
           writeln('*  Tidak Ditemukan   *');
           writeln('**********************');
        end
          else
            begin
               with data_karyawan[idx] do
               begin
                   write('Golongan (A\B\C)      :');readln(gol);
                   write('Satus (Nikah\Belum)   :');readln(status);
               end;
               proses(data_gapok,data_karyawan[idx]);
            end;

         writeln('Edit data lagi (y/n) : '); readln(input_data);

     until((input_data='n') or (input_data='N'));
end;

procedure delete(var data_karyawan:Tarr_tdata_karyawan;var jml_data:integer);
var
  input_data : char;
  nama : string;
  idx,i:integer;
begin
  clrscr;
  writeln('*******************');
  writeln('*  Delete  Data   *');
  writeln('*******************');
     repeat
        writeln;
        write('Nama Karyawan         :');readln(nama);
        idx:=cari_data_karyawan(nama,data_karyawan,jml_data);

        if idx=0 then
        begin
           writeln('**********************');
           writeln('*  Tidak Ditemukan   *');
           writeln('**********************');
        end
          else
            begin
               for i:=idx to jml_data-1 do
               begin
                   data_karyawan[i]:=data_karyawan[i+1];
               end;
               jml_data:=jml_data-1;
            end;

         writeln('Delete data lagi (y/n) : ');readln(input_data);

         input_data:=readkey;

     until((input_data='n') or (input_data='N'));
end;

procedure output(data_karyawan:Tarr_tdata_karyawan;jml_data:integer);
var
  i:integer;
begin
     clrscr;
     writeln('*****************');
     writeln('*  Output Data  *');
     writeln('*****************');
     writeln('---------------------------------------------------------------------------------');
     writeln(' No | Nama | Gol | Status | Gaji Pokok | Tunjangan | Potongan Iuran | Gaji Bersih');
     writeln('---------------------------------------------------------------------------------'); 
     for i:=1 to jml_data do
     begin
        write(i);
        with data_karyawan[i] do
        begin
        write(' | ');
        write(nama);
        write(' | ');
        write(gol);
        write(' | ');
        write(status);
        write(' | ');
        write('Rp.',gapok_tunja.gapok:6);
        write(' | ');
        write('Rp.',gapok_tunja.tunja:6);
        write(' | ');
        write('Rp.',pot_iuran:6:0);
        write(' | ');
        write('Rp.',gaber:6:0);
        write(' | ');
          
        end;
        writeln;
     end;
     readkey;
end;

function menu:char;
begin
  clrscr;
  writeln('*******************');
  writeln('* 1.Input  Data   *');
  writeln('* 2.Output Data   *');
  writeln('* 3.Edit Data     *');
  writeln('* 4.Delete Data   *');
  writeln('*******************');
  writeln('* 0.Keluar        *');
  writeln('*******************');
  write('Pilih menu : '); menu := readkey;
end;

var
  data_gapok : Tarr_Tgapok;
  jml_data : integer;
  no_menu : char;
  data_karyawan : tarr_tdata_karyawan;
begin
       isi_data_gapok(data_gapok);
       jml_data:=0;
       repeat
         no_menu:=menu;
         case no_menu of
          '1' : input(data_gapok,data_karyawan,jml_data);
          '2' : output(data_karyawan,jml_data);
          '3' : edit(data_gapok,data_karyawan,jml_data);
          '4' : delete(data_karyawan,jml_data);
         end;
       until(no_menu='0');
end.
